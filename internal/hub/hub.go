package hub

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"pdf/psc/config"
)

type hub struct {
	uri string
	tkn *token

	s config.Signature
}

func New(c config.Hub, s config.Signature) (Hub, error) {
	tkn, err := newToken(c.CloudUrl, c.ClientID, c.ClientSecret)
	if err != nil {
		return nil, err
	}

	return hub{
		uri: c.Url,
		tkn: tkn,
		s:   s,
	}, nil
}

func (h hub) PdfSignature(document []byte, pscUrl, pscToken string) ([]byte, error) {
	// URL para acesso a API
	uri := h.uri + "/fw/v1/pdf/kms/lote/assinaturas"

	// Multipart body writer
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Adiciona documento. Para assinaturas em lote, o valor do indice deve ser incrementado (documento[1], documento[2], ...)
	w, err := writer.CreateFormFile("documento[0]", "documento.pdf")
	if err != nil {
		return nil, err
	}
	_, err = w.Write(document)
	if err != nil {
		return nil, err
	}

	// Adiciona informações sobre o tipo de assinatura
	dadosAssinaturaBytes, err := json.Marshal(newDadosAssinatura(pscUrl, pscToken, h.s.Perfil, h.s.AlgoritmoHash, h.s.Razao, h.s.Contato, h.s.Local, h.s.TipoRestricao, h.s.TipoRetorno, h.s.Restructure))
	if err != nil {
		return nil, err
	}
	err = writer.WriteField("dados_assinatura", string(dadosAssinaturaBytes))
	if err != nil {
		return nil, err
	}

	// Adiciona imagem, caso configurada
	if imageConfig := h.s.Image; imageConfig != nil {
		imageBytes, err := base64.StdEncoding.DecodeString(imageConfig.Base64)
		if err != nil {
			return nil, err
		}
		w, err = writer.CreateFormFile("imagem[0]", "imagem")
		if err != nil {
			return nil, err
		}
		_, err = w.Write(imageBytes)
		if err != nil {
			return nil, err
		}

		confImagemBytes, err := json.Marshal(newConfiguracaoImagem(imageConfig.Altura, imageConfig.Largura, imageConfig.CoordenadaInicialX, imageConfig.CoordenadaInicialY, imageConfig.Posicao, imageConfig.Pagina))
		if err != nil {
			return nil, err
		}
		err = writer.WriteField("configuracao_imagem", string(confImagemBytes))
		if err != nil {
			return nil, err
		}
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		return nil, err
	}
	// Pega token JWT utilizado para requisição
	accessToken, err := h.tkn.getToken()
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", accessToken)
	req.Header.Set("kms_type", "PSC")
	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.Body == nil {
		return nil, fmt.Errorf("hub response body returned nil and status %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		// Ocorreu algum erro na requisição
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, fmt.Errorf("error reading hub response body: %s", err.Error())
		}
		return nil, errors.New(string(b))

	}

	var signedDocument []byte
	switch h.s.TipoRetorno {
	case "BASE64":
		// Decodifica resposta Base64 da API
		var response base64Response
		err = json.NewDecoder(resp.Body).Decode(&response)
		if err != nil {
			return nil, err
		}
		signedDocument, err = base64.StdEncoding.DecodeString(response[0])
		if err != nil {
			return nil, err
		}
	default: // "LINK"
		// Decodifica resposta Link da API, buscando o documento assinado
		var response linkResponse
		err = json.NewDecoder(resp.Body).Decode(&response)
		if err != nil {
			return nil, err
		}
		link := response.Documentos[0].Links[0]

		r, err := http.Get(link.Href)
		if err != nil {
			return nil, err
		}
		if r.StatusCode != http.StatusOK || r.Body == nil {
			return nil, fmt.Errorf("error getting response from link. Status '%d', has body? %t", r.StatusCode, r.Body != nil)
		}
		signedDocument, err = io.ReadAll(r.Body)
		if err != nil {
			return nil, err
		}
	}
	return signedDocument, nil
}
