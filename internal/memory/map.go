package memory

type mem map[string]any

func New() Memory {
	return make(mem)
}

func (m mem) Save(id string, value any) {
	m[id] = value
}

func (m mem) Get(id string) (any, bool) {
	v, ok := m[id]
	return v, ok
}
