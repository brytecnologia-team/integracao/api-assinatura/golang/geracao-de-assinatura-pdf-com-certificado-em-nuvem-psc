package config

type Psc struct {
	Url          string
	RedirectUrl  string
	ClientID     string
	ClientSecret string
	Scope        string
}

type Hub struct {
	Url          string
	CloudUrl     string
	ClientID     string
	ClientSecret string
}

type Signature struct {
	Perfil        string
	AlgoritmoHash string
	Razao         string
	Contato       string
	Local         string
	TipoRestricao string
	TipoRetorno   string
	Restructure   bool

	Image *SignatureImage
}

type SignatureImage struct {
	Base64             string
	Altura             float64
	Largura            float64
	CoordenadaInicialX float64
	CoordenadaInicialY float64
	Posicao            string
	Pagina             string
}

var Config = struct {
	Port      int
	Hub       Hub
	Psc       Psc
	Signature Signature
}{
	Port: 8000,

	Signature: Signature{
		Perfil:        "TIMESTAMP",                                        // BASIC, TIMESTAMP, COMPLETE, ADRB, ADRT, ADRV, ADRC, ADRA, ETSI_B, ETSI_T, ETSI_LT e ETSI_LTA
		AlgoritmoHash: "SHA256",                                           // SHA256, SHA512
		Razao:         "",                                                 // Opcional
		Contato:       "",                                                 // Opcional
		Local:         "",                                                 // Opcional
		TipoRestricao: "PERMITIR_COMENTARIOS_E_PREENCHIMENTO_FORMULARIOS", // Opcional: NENHUMA_RESTRICAO, PERMITIR_COMENTARIOS_E_PREENCHIMENTO_FORMULARIOS, PERMITIR_SOMENTE_PREENCHIMENTO_FORMULARIOS, DESABILITAR_QUALQUER_ALTERACAO
		TipoRetorno:   "LINK",                                             // Opcional: LINK, BASE64
		Restructure:   true,                                               // Opcional

		Image: &SignatureImage{ // Opcional, utilizado somente para imagens
			Base64:             "iVBORw0KGgoAAAANSUhEUgAAAB4AAAAPCAYAAADzun+cAAAACXBIWXMAAAsSAAALEgHS3X78AAABmElEQVQ4ja1V3W3CMBD+ijoAGzRPfqYbhAmgE5BKHgAmIExA+x6JZIKyQegGPPsp3YARqrPvkotJBKj9JMv25ez77seXJxRuCmDGg9aCE4AzrLm0ksIlAGikSq9hvTMeABmmS+qRI2R0A2tKNpwD2I7okuH3ewlMon3Dnja8pwgc2NNboIh93evzc7SvYE3O3u0BrFmeKDKCOaw5MamadRIUjgismAhgzbw9UTiJ7Dn2WBSmfJHgMqgXLpYoCaROUj9CKsGEUqmP2OMtChfnsBzJ2wyFo3kBIFNyIvKtorVgYrogq2GP+1gy2xh7DvFayT98BKw5qtQseV61xKy5CnXpcxfGToVurJI1yOhG7Y88JxzumZbHhn98wYSRK/kU1xCCgiyq/k+1Pqh1NWT4xbML4/bTIILkaUfuoL41Pa8D2kYTG844b7XKTcw+xk5VfVfFw+fa/YQPjT0XYvfGxTLmdehuHbbqm25Gl7YD+pap0fXtfo9+FMFruUuI7HTd9A3/F0KHin8kr9qZe97xX0FpovbaRRDALxnAhd1SgHC5AAAAAElFTkSuQmCC",
			Altura:             50,
			Largura:            50,
			CoordenadaInicialX: 10,
			CoordenadaInicialY: 10,
			Posicao:            "INFERIOR_ESQUERDO", // INFERIOR_DIREITO, INFERIOR_ESQUERDO, SUPERIOR_DIREITO e SUPERIOR_ESQUERDO
			Pagina:             "PRIMEIRA",          // PRIMEIRA, ULTIMA e TODAS
			// NumeroPagina: 1,
		},
	},

	Hub: Hub{
		Url:          "<HUB_URL>",   // Ex: Produção: https://hub2.bry.com.br, Homologação: https://hub2.hom.bry.com.br
		CloudUrl:     "<CLOUD_URL>", // Ex: Produção: https://cloud.bry.com.br, Homologação: https://cloud-hom.bry.com.br
		ClientID:     "<CLOUD_CLIENT_ID>",
		ClientSecret: "<CLOUD_CLIENT_SECRET>",
	},

	Psc: Psc{
		Url:          "<PSC_URL>",      // Verifique com seu provedor de acesso ao certificado em nuvem sua URL base
		RedirectUrl:  "<REDIRECT_URL>", // Ex: http://localhost:8000/psc
		ClientID:     "<PSC_CLIENT_ID>",
		ClientSecret: "<PSC_CLIENT_SECRET>",
		Scope:        "signature_session", // Valores disponíveis: single_signature | multi_signature | signature_session
	},
}
