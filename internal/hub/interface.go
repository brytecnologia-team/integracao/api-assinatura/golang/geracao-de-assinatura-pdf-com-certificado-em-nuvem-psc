package hub

type Hub interface {
	PdfSignature(document []byte, pscUri, pscToken string) (signedDocument []byte, err error)
}
