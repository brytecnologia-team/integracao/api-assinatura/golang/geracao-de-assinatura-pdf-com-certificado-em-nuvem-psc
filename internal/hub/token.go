package hub

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

type token struct {
	url          string
	clientID     string
	clientSecret string

	accessToken string
	expires     time.Time

	m sync.Mutex
}

func newToken(url, clientID, clientSecret string) (*token, error) {
	tkn := &token{
		url:          url,
		clientID:     clientID,
		clientSecret: clientSecret,
	}

	_, err := tkn.getToken()
	if err != nil {
		return nil, err
	}
	return tkn, nil
}

func (t *token) getToken() (string, error) {
	t.m.Lock()
	defer t.m.Unlock()

	// Verifica se token atual está expirado, reutilizando tokens válidos
	if t.expires.Before(time.Now()) {
		// Gera um novo token
		err := t.generateTokenToken()
		if err != nil {
			return "", err
		}
	}

	return t.accessToken, nil
}

func (t *token) generateTokenToken() error {
	uri := fmt.Sprintf("%s/token-service/jwt", t.url)

	data := url.Values{}
	data.Set("grant_type", "client_credentials")
	data.Set("client_id", t.clientID)
	data.Set("client_secret", t.clientSecret)

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if resp.Body == nil {
		return fmt.Errorf("token response body returned nil and status %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("error reading token response body: %s", err.Error())
		}
		return errors.New(string(b))
	}

	var response tokenResponse
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return err
	}

	t.accessToken = response.AccessToken
	t.expires = time.Now().Add(time.Second * time.Duration(response.ExpiresIn))
	return nil
}
