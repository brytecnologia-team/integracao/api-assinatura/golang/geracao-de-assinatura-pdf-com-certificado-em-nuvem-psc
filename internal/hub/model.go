package hub

type dadosAssinatura struct {
	KmsData       *kmsData `json:"kms_data"`
	Perfil        string   `json:"perfil"`
	AlgoritmoHash string   `json:"algoritmoHash"`
	Razao         string   `json:"razao,omitempty"`
	Contato       string   `json:"contato,omitempty"`
	Local         string   `json:"local,omitempty"`
	TipoRestricao string   `json:"tipoRestricao,omitempty"`
	TipoRetorno   string   `json:"tipoRetorno,omitempty"`
	Restructure   bool     `json:"restructure"`
}

func newDadosAssinatura(url, token, perfil, algoritmoHash, razao, contato, local, tipoRestricao, tipoRetorno string, restructure bool) *dadosAssinatura {
	data := newKmsData(url, token)
	return &dadosAssinatura{
		KmsData:       data,
		Perfil:        perfil,
		AlgoritmoHash: algoritmoHash,
		Razao:         razao,
		Contato:       contato,
		Local:         local,
		TipoRestricao: tipoRestricao,
		TipoRetorno:   tipoRetorno,
		Restructure:   restructure,
	}
}

type kmsData struct {
	Url   string `json:"url"`
	Token string `json:"token"`
}

func newKmsData(url, token string) *kmsData {
	return &kmsData{
		Url:   url,
		Token: token,
	}
}

type configuracaoImagem struct {
	Altura             float64 `json:"altura"`
	Largura            float64 `json:"largura"`
	CoordenadaInicialX float64 `json:"coordenadaX"`
	CoordenadaInicialY float64 `json:"coordenadaY"`
	Posicao            string  `json:"posicao,omitempty"`
	Pagina             string  `json:"pagina,omitempty"`
}

func newConfiguracaoImagem(altura, largura, coordX, coordY float64, posicao, pagina string) *configuracaoImagem {
	return &configuracaoImagem{
		Altura:             altura,
		Largura:            largura,
		CoordenadaInicialX: coordX,
		CoordenadaInicialY: coordY,
		Posicao:            posicao,
		Pagina:             pagina,
	}
}

type base64Response []string

type linkResponse struct {
	Documentos []struct {
		Links []struct {
			Href string `json:"href"`
		} `json:"links"`
	} `json:"documentos"`
}

type tokenResponse struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}
